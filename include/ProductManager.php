<?php

class ProductManager extends dbh
{
    private $database;

    public function __construct()
    {
        $this->database = new dbh();
    }

    // Create a Product by a specified productType
    public function createProduct(ProductFactory $request): Product
    {
        return $request;
    }

    // Create an array of all Products from database
    public function listProducts()
    {
        $datas = $this->getAll();
        if ($datas) {
            foreach ($datas as $data) {
                $temp_product = $this->createProduct(new $data['productType']($data));
                if ($temp_product) {
                    $products[] = $temp_product;
                }
            }
            return (!empty($products) ? $products : []);
        }
    }

    // Create New SKU for ADDing New Product
    public function create_random_sku(int $length = 8, string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'): string
    {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }

        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;

        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    // Check if sku is unique
    public function check_sku_availability($data)
    {
        $result = $this->getAll();
        $count = 0;
        foreach ($result as $product) {
            if ($product['sku'] == $data) {
                $count += 1;
            }
        }
        return $count;
    }

    // common validator
    public function dataValidation(array $data = null): array
    {
        $errors = array();
        $test_product = $this->createProduct(new $data['productType']);
        $errors['state'] = true;
        $count = $this->check_sku_availability($data['sku']);

        //check SKU format
        if (empty($data['sku'])) {
            $errors['sku'] = 'SKU must be letters or/and numbers.';
            $errors['state'] = false;
        }
        // check SKU availability
        elseif ($count != 0) {
            $errors['sku'] = 'SKU has to be unique for every product.';
            $errors['state'] = false;
        } elseif ($test_product->validateSKU($data)) {
            $errors['sku'] = '';
            $errors['state'] = ($errors['state'] != false) ? true : false;
        } else {
            $errors['state'] = false;
        }

        // check name format
        if (empty($data['name']) || ($test_product->validateName($data) == false)) {
            $errors['name'] = 'Name must be letters or/and numbers.';
            $errors['state'] = false;
        } else {
            $errors['name'] = '';
            $errors['state'] = ($errors['state'] != false) ? true : false;
        }

        // check price format
        if (empty($data['price']) || ($test_product->validatePrice($data) == false)) {
            $errors['price'] = 'Price must be numbers only.';
            $errors['state'] = false;
        } else {
            $errors['price'] = '';
            $errors['state'] = ($errors['state'] != false) ? true : false;
        }

        // check property format
        $results = $test_product->validateProperty($data);
        foreach ($results as $k => $v) {
            if ($k != 'state')
                $errors[$k] = $v;
            else
                $errors['state'] = ($errors['state'] != false) ? (($v != false) ? true : false) : false;
        }
        return $errors;
    }

    // Query Functions
    // 1. Get All Data from database
    public function getAll()
    {
        $sql = "SELECT * FROM productlist";
        $result = $this->database->db_operation($sql);

        $numRows = $result->num_rows;
        if ($numRows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }
    }

    // 2. Specify the location of Some Products in database to be deleted
    public function where($product_sql)
    {
        if (count($product_sql) == 1) {
            $arr = $product_sql;
            $sql = "DELETE FROM productlist WHERE sku='$arr[0]'";
        } else {
            $arr = implode("' ,'", $product_sql);
            $sql = "DELETE FROM productlist WHERE sku IN ('$arr')";
        }
        return $sql;
    }

    // 3. Delete one or some products form database 
    public function deleteProduct($products_sku)
    {
        $sql = $this->where($products_sku);
        $result = $this->database->db_operation($sql);
        return $result;
    }

    // 4. insert or add a new Product to database
    public function insertProduct($data_keys, $data_values): bool
    {
        $sql = "insert into productlist ($data_keys) values($data_values)";
        return $this->database->db_operation($sql);
    }
}
