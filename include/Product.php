<?php

// Abstract Class - Main Creator
abstract class Product implements ProductFactory
{
    // Products main Parameters
    protected $sku;
    protected $product_name;
    protected $productType;
    protected $price;
    protected $property;

    // Parent Constructor
    function __construct(array $data = null)
    {
        $this->setSKU((isset($data['sku']) ? $data['sku'] : false));
        $this->setName((isset($data['name']) ? $data['name'] : false));
        $this->setPrice((isset($data['price']) ? $data['price'] : false));
    }

    // Main Parameters Setters
    protected function setSKU($sku_data)
    {
        $this->sku = $sku_data;
    }
    protected function setName($name_data)
    {
        $this->product_name = $name_data;
    }
    protected function setPrice($price_data)
    {
        $this->price = $price_data;
    }

    // Main Parameters Getters 
    public function getSKU()
    {
        return $this->sku;
    }
    public function getName()
    {
        return $this->product_name;
    }
    public function getType()
    {
        return $this->productType;
    }
    public function getPrice()
    {
        return $this->price . " $";
    }

    // Product Validator
    public function validateSKU(array $data): bool
    {
        if ((isset($data['sku'])) && (!empty($data['sku'])) && (strlen($data['sku']) > 0) &&  (!preg_match('/\s/', $data['sku']))) {
            return true;
        } else {
            return false;
        }
    }

    public function validateName(array $data): bool
    {
        if ((isset($data['name'])) && (!empty($data['name'])) && (strlen($data['name']) > 0) &&  (!preg_match('/\s/', $data['name']))) {
            return true;
        } else {
            return false;
        }
    }

    public function validatePrice(array $data): bool
    {
        if ((strlen($data['price']) > 0) && (floatval($data['price']) >= 0) && is_numeric($data['price'])) {
            return true;
        } else {
            return false;
        }
    }

    // Product Special Properties Details
    abstract function getProperty();

    abstract function validateProperty(array $data = null);

    public function validateNumber($data, $key)
    {
        $errors = array();
        if (!empty($data[$key]) && (strlen($data[$key]) > 0) && (floatval($data[$key]) >= 0) && is_numeric($data[$key])) {
            $errors[$key] = '';
            $errors['state'] = true;
        } else {
            $errors[$key] = 'Value must be numbers only.';
            $errors['state'] = false;
        }
        return $errors;
    }
}
