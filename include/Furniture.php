<?php

// Concrete Product: Furniture
class Furniture extends Product
{
    // Product Special Parameters
    protected $productType = "Furniture";
    protected $height;
    protected $width;
    protected $length;

    // Product Constructor
    function __construct(array $data  = null)
    {
        parent::__construct($data);
        $this->setHeight((isset($data['height']) ? $data['height'] : false));
        $this->setWidth((isset($data['width']) ? $data['width'] : false));
        $this->setLength((isset($data['length']) ? $data['length'] : false));
    }

    // Product Special Parameters Setters
    private function setHeight($height_data)
    {
        $this->height = $height_data;
    }
    private function setWidth($width_data)
    {
        $this->width = $width_data;
    }
    private function setLength($length_data)
    {
        $this->length = $length_data;
    }

    // Product Special Parameters Getters
    public function getHeight()
    {
        return $this->height;
    }
    public function getWidth()
    {
        return $this->width;
    }
    public function getLength()
    {
        return $this->length;
    }

    // Product Validator
    public function validateProperty(array $data = null): array
    {
        $temp = array();
        $errors = array();
        // Check height
        $errors = $this->validateNumber($data, 'height');

        // Check width
        $temp = $this->validateNumber($data, 'width');
        $errors['width'] = $temp['width'];
        $errors['state'] = ($temp['state'] != false) ? true : false;

        // Check length
        $temp = $this->validateNumber($data, 'length');
        $errors['length'] = $temp['length'];
        $errors['state'] = ($temp['state'] != false) ? true : false;

        return $errors;
    }

    // Product Special Properties Details
    public function getProperty()
    {
        return "Dimension: " . $this->getHeight() . "x" . $this->getWidth() . "x" . $this->getLength();
    }

    public function printClassName()
    {
        echo "{Result of the Furniture concrete product}";
    }
}
