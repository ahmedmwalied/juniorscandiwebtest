<?php

// Concrete Product: DVD
class DVD extends Product
{
    // Product Special Parameters
    protected $productType = "DVD";
    protected $size;

    // Product Constructor
    function __construct(array $data = null)
    {
        parent::__construct($data);
        $this->setSize((isset($data['size']) ? $data['size'] : false));
    }

    // Product Special Parameters Setters
    private function setSize($size_data)
    {
        $this->size = $size_data;
    }

    // Product Special Parameters Getters
    public function getSize()
    {
        return $this->size;
    }

    // Product Validator
    public function validateProperty(array $data = null): array
    {
        $errors = array();
        $errors = $this->validateNumber($data, 'size');
        return $errors;
    }


    // Product Special Properties Details
    public function getProperty()
    {
        return "Size: " . $this->getSize() . "MB";
    }

    public function printClassName()
    {
        echo "{Result of the DVD concrete product}";
    }
}
