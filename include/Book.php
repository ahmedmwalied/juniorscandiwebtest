<?php

// Concrete Product: Book
class Book extends Product
{
    // Product Special Parameters
    protected $productType = "Book";
    protected $weight;

    // Product Constructor
    function __construct(array $data = null)
    {
        parent::__construct($data);
        $this->setWeight((isset($data['weight']) ? $data['weight'] : false));
    }

    // Product Special Parameters Setters
    private function setWeight($weight_data)
    {
        $this->weight = $weight_data;
    }

    // Product Special Parameters Getters
    public function getWeight()
    {
        return $this->weight;
    }

    // Product Validator
    public function validateProperty(array $data = null): array
    {
        $errors = array();
        $errors = $this->validateNumber($data, 'weight');
        return $errors;
    }


    // Product Special Properties Details
    public function getProperty()
    {
        return "Weight: " . $this->getWeight() . "kg";
    }

    public function printClassName()
    {
        echo "{Result of the Book concrete product}";
    }
}
