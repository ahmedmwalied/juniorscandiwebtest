<?php

// The Product interface declares all operations that all products must implements
interface ProductFactory
{
    public function getProperty();
    public function validateProperty(array $data = null);
}
