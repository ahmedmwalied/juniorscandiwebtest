<?php

$request = $_SERVER['REQUEST_URI'];

if ($request == '' || $request == '/' || $request == '/index.php') {
    header('Location: product-list.php');
} elseif ($request == '/add-product') {
    header('Location: add-product.php');
} else {
    header('Location: product-list.php');
}
