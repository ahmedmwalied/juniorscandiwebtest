<?php
class dbh
{
    private $servername;
    private $username;
    private $password;
    private $dbname;
    protected $db_con;


    function __construct()
    {
        $this->servername = 'localhost';
        $this->username = 'root';
        $this->dbname = 'productslist';
        $this->password = '';

        // $this->servername = 'localhost';
        // $this->username = 'id17542745_rootuser';
        // $this->dbname = 'id17542745_productslist';
        // $this->password = 'juniorTest50@$ahmedWalied';

        $this->db_con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        if (!$this->db_con) {
            die(mysqli_error($this->db_con));
        }
    }

    public function db_operation($sql)
    {
        return mysqli_query($this->db_con, $sql);
    }
}
