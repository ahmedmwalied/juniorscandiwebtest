<?php
include 'db/dbh.php';
include 'include/ProductFactory.php';
include 'include/Product.php';
include 'include/Book.php';
include 'include/DVD.php';
include 'include/Furniture.php';
include 'include/ProductManager.php';

$value = array(
    'post' => '',
    'sku' => '',
    'name' => '',
    'price' => '',
    'productType' => '',
    'size' => '',
    'height' => '',
    'width' => '',
    'length' => '',
    'weight' => ''
);

$errors = array(
    'sku' => '',
    'name' => '',
    'price' => '',
    'productType' => '',
    'size' => '',
    'height' => '',
    'width' => '',
    'length' => '',
    'weight' => ''
);

$product_manager = new ProductManager();

if (isset($_POST['Save'])) {
    $result = false;
    $value['post'] = 'post';
    foreach ($_POST as $k => $v) {
        $value[$k] = trim($v);
    }
    if (!empty($_POST['productType']) && in_array($_POST['productType'], array('DVD', 'Book', 'Furniture'))) {
        $results = $product_manager->dataValidation($value);
        foreach ($results as $k => $v) {
            $errors[$k] = $v;
        }
    } else {
        $errors['productType'] = 'You have to choose one of the valid categories!';
        $errors['state'] = false;
    }

    if ($errors['state'] != false) {
        $parameters = "";
        $parameters_value = "";

        foreach ($_POST as $k => $v) {
            if ($k != 'Save') {
                if (!empty($v)) {
                    $parameters .= "$k, ";
                    $parameters_value .= "'$v', ";
                }
            }
        }

        $parameters = substr(trim($parameters), 0, -1);
        $parameters_value = substr(trim($parameters_value), 0, -1);
        $saved = $product_manager->insertProduct($parameters, $parameters_value);

        if ($saved) {
            header('Location: index.php');
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link crossorigin="anonymous" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <title>Product Add</title>
</head>

<body>
    <!-- Nav-->
    <nav class="navbar navbar-light bg-gradient position-relative">
        <div class="container">
            <a class="navbar-brand navbar-expand mt-3 mb-1" href="#">
                <h1>Product Add</h1>
            </a>

            <!-- Start Form -->
            <div class="container position-absolute top-0 mt-4">
                <form id="product_form" method="post" class="form-inline mt-2" name="product_form">
                    <div class="">
                        <div class="d-grid gap-3 d-lg-flex justify-content-end mb-4 mx-4 pb-2">
                            <button id="submit" type="submit" class="btn btn-light btn-outline-success" name="Save">
                                Save
                            </button>

                            <a name="Cancel" href="index.php" class="btn btn-light btn-outline-success" role="button">Cancel</a>
                        </div>
                    </div>
                    <hr />


                    <div class="container">
                        <!-- sku-->
                        <div class="form-group row mt-5 mb-4" id="form">
                            <label for="sku" class="col-md-1 col-form-label">SKU</label>

                            <div class="col-sm-4">
                                <input name="sku" type="text" id="sku" class="form-control" placeholder="#SKU" value="<?php if (!empty($value['post']) && !empty($value['sku'])) {
                                                                                                                            echo htmlspecialchars($value['sku'], ENT_QUOTES);
                                                                                                                        } else {
                                                                                                                            echo $product_manager->create_random_sku(8);
                                                                                                                        } ?>" required>
                            </div>
                            <div class=" col-sm-4" style="color:red">
                                <?php if (!empty($value['post']) && $errors['state'] == false) {
                                    echo $errors['sku'];
                                } ?>
                            </div>
                        </div>
                        <!-- End SKU-->

                        <!-- product name -->
                        <div class="form-group row mb-4">
                            <label class="col-md-1 col-form-label">Name</label>
                            <div class="col-sm-4">
                                <input name="name" type="text" id="name" class="form-control" placeholder="#Name" value="<?php if (!empty($value['post']) && !empty($value['name'])) {
                                                                                                                                echo htmlspecialchars($value['name'], ENT_QUOTES);
                                                                                                                            } ?>" required>
                            </div>
                            <div class="col-sm-4" style="color:red">
                                <?php if (!empty($value['post']) && $errors['state'] == false) {
                                    echo $errors['name'];
                                } ?>
                            </div>
                        </div>
                        <!-- End product name -->

                        <!-- price -->
                        <div class="form-group row mb-4">
                            <label class="col-md-1 col-form-label">Price ($)</label>
                            <div class="col-sm-4">
                                <input name="price" type="number" id="price" class="form-control" step="0.01" placeholder="#Price" value="<?php if (!empty($value['post']) && !empty($value['price'])) {
                                                                                                                                                echo htmlspecialchars($value['price'], ENT_QUOTES);
                                                                                                                                            } ?>" required>
                            </div>
                            <div class="col-sm-4" style="color:red">
                                <?php if (!empty($value['post']) && $errors['state'] == false) {
                                    echo $errors['price'];
                                } ?>
                            </div>
                        </div>
                        <!-- End price -->

                        <!-- Select Product Type -->
                        <div class="form-group row mb-4">
                            <label for="type" class="col-md-1 col-form-label"> Type</label>
                            <div class="col-sm-4">
                                <select name="productType" id="productType" class="form-control" placeholder="#product type" />
                                <option value="" selected disabled>---</option>
                                <option value="DVD">DVD</option>
                                <option value="Furniture">Furniture</option>
                                <option value="Book">Book</option>
                                </select>
                            </div>
                            <div class="col-sm-4" style="color:red">
                                <?php if (!empty($value['post']) && $errors['state'] == false) {
                                    echo $errors['productType'];
                                } ?>
                            </div>
                        </div>

                        <div class="form-group mt-4 mb-4" id="parameters">
                        </div>

                        <hr>
                    </div>
                </form>
                <!-- End Form -->
            </div>
    </nav>
    <!-- Nav End-->

    <footer class="footer mt-auto py-3 bg-light text-center text-lg-start">
        <div class="fixed-bottom text-center p-3" style="background-color: gray;">
            Scandiweb Test Assignment
        </div>
    </footer>

</body>

</html>

<script>
    $(document).ready(() => {
        $("#productType").change(function() {
            switch (this.value) {
                case "DVD":
                    $("#parameters").html(`
                        <!-- Product Type Options Content -->
                        <div class="content">
                            <!-- DVD SiZe Content -->
                            <div class="form-group row mb-4">
                            <label for="size" class="col-sm-2 col-form-label text-end">Size (MB)</label>
                            <div class="col-sm-4">
                                <input name="size" type="number" id="size" class="form-control" step="0.01" placeholder="#size" value="<?php if (!empty($value['post']) && !empty($value['size'])) {
                                                                                                                                            echo htmlspecialchars($value['size'], ENT_QUOTES);
                                                                                                                                        } ?>" required>
                            </div>

                            <div class=" col-sm-4" style="color:red">
                                <?php if (!empty($value['post']) && $errors['state'] == false) {
                                    echo $errors['size'];
                                } ?>
                            </div>
                        </div>
                        <!-- End DVD SiZe Content -->
                        `);
                    break;

                case "Book":
                    $("#parameters").html(`
                        <!-- Book SiZe Content -->
                        <div class="form-group row mb-4">
                            <label for="weight" class="col-sm-2 col-form-label text-end">Weight (kg)</label>
                            <div class="col-sm-4">
                                <input name="weight" type="number" id="weight" class="form-control" step="0.01" placeholder="#weight" value="<?php if (!empty($value['post']) && !empty($value['weight'])) {
                                                                                                                                                    echo htmlspecialchars($value['weight'], ENT_QUOTES);
                                                                                                                                                } ?>" required>
                            </div>
                            
                            <div class=" col-sm-4" style="color:red">
                                <?php if (!empty($value['post']) && $errors['state'] == false) {
                                    echo $errors['weight'];
                                } ?>
                            </div>
                        </div>
                        <!-- End Book SiZe Content -->
                        `);
                    break;

                case "Furniture":
                    $("#parameters").html(`
                        <!-- Furniture parameters Content -->
                        <div class="form-group row mb-4">
                            <label for="height" class="col-sm-2 col-form-label text-end">Height (cm)</label>
                            <div class="col-sm-4">
                                <input name="height" type="number" id="height" class="form-control" step="0.01" placeholder="#height" value="<?php if (!empty($value['post']) && !empty($value['height'])) {
                                                                                                                                                    echo htmlspecialchars($value['height'], ENT_QUOTES);
                                                                                                                                                } ?>" required>
                            </div>

                            <div class=" col-sm-4" style="color:red">
                                <?php if (!empty($value['post']) && $errors['state'] == false) {
                                    echo $errors['height'];
                                } ?>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label for="width" class="col-sm-2 col-form-label text-end">Width (cm)</label>
                            <div class="col-sm-4">
                                <input name="width" type="number" id="width" class="form-control" step="0.01" placeholder="#width" value="<?php if (!empty($value['post']) && !empty($value['width'])) {
                                                                                                                                                echo htmlspecialchars($value['width'], ENT_QUOTES);
                                                                                                                                            } ?>" required>
                            </div>

                            <div class=" col-sm-4" style="color:red">
                                <?php if (!empty($value['post']) && $errors['state'] == false) {
                                    echo $errors['width'];
                                } ?>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label for="length" class="col-sm-2 col-form-label text-end">Length (cm)</label>
                            <div class="col-sm-4">
                                <input name="length" type="number" id="length" class="form-control" step="0.01" placeholder="#length" value="<?php if (!empty($value['post']) && !empty($value['length'])) {
                                                                                                                                                    echo htmlspecialchars($value['length'], ENT_QUOTES);
                                                                                                                                                } ?>" required>
                            </div>

                            <div class=" col-sm-4" style="color:red">
                                <?php if (!empty($value['post']) && $errors['state'] == false) {
                                    echo $errors['length'];
                                } ?>
                            </div>
                        </div>
                        <!-- End Furniture parameters Content -->
                        `);
                    break;
            }
        });
    });
</script>