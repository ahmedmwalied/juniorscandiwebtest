<?php
include 'db/dbh.php';
include 'include/ProductFactory.php';
include 'include/Product.php';
include 'include/Book.php';
include 'include/DVD.php';
include 'include/Furniture.php';
include 'include/ProductManager.php';

if (isset($_POST['Delete'])) {
    foreach ($_POST as $key => $value) {
        $keys = array();
        foreach ($_POST as $key => $value) {
            if ($key != 'Delete') {
                $keys[] = "$value";
            }
        }
    }

    if (count($keys) != 0) {
        $terminated = new ProductManager();
        $terminated->deleteProduct($keys);
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous" />

    <title>Product List</title>
</head>

<body>
    <!-- Nav-->
    <nav class="navbar navbar-light bg-gradient position-relative">
        <div class="container">
            <a class="navbar-brand navbar-expand mt-3 mb-1" href="#">
                <h1>Product Add</h1>
            </a>

            <!-- Start Form -->
            <div class="container position-absolute top-0 mt-4">
                <form id="#product_form" method="post" class="form-inline mt-2">
                    <div class="">
                        <div class="d-grid gap-3 d-lg-flex justify-content-end mb-4 mx-4 pb-2">
                            <a href="add-product.php" id="#ADD" name="ADD" class="btn btn-light btn-outline-success" role="button">ADD</a>

                            <button id="submit" type="submit" class="btn btn-danger btn-outline text-black" name="Delete">
                                MASS DELETE
                            </button>
                        </div>
                    </div>

                    <hr />

                    <div class="row text-center g-4">
                        <div class="row mt-5">
                            <?php
                            $productmanager = new ProductManager();
                            $products = $productmanager->listProducts();

                            if (!empty($products)) {
                                foreach ($products as $product) {
                            ?>
                                    <div class="card col-md-2 m-3" style="width: 18rem;">
                                        <div class="card-body">
                                            <div class="checkbox position-absolute start-20">
                                                <label><input class="delete-checkbox" type="checkbox" name="<?php echo $product->getSKU(); ?>" value="<?php echo $product->getSKU(); ?>" /></label>
                                            </div>
                                            <h5 class="card-title mt-4"><?php echo $product->getSKU(); ?></h5>
                                            <h5 class="card-title"><?php echo $product->getName(); ?></h5>
                                            <h6 class="card-subtitle mb-3 text-muted"><?php echo $product->getPrice(); ?></h6>
                                            <h6 class="card-subtitle mb-4 text-muted"><?php echo $product->getProperty(); ?></h6>
                                        </div>
                                    </div>
                            <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </nav>
    <!-- Nav End-->

    <footer class="footer mt-auto py-3 bg-light text-center text-lg-start">
        <div class="fixed-bottom text-center p-3" style="background-color: gray;">
            Scandiweb Test Assignment
        </div>
    </footer>
</body>

</html>